# FruitsApp

FruitsApp is a web application with a CRUD of fruits. This app was created as part of the 2Coders Access Test. The backend was made in Laravel and the frontend in Angular.

NOTE: This project was developed and tested on Windows.

## Installation
To install the application simply follow these steps:
1. Download the source code in a local folder of your choice.
2. Make sure you have the Docker daemon running.
3. Open a prompt in the repository root folder.
4. Execute the following command:
```bash
 docker-compose build
```
 
## Deployment
In order to deploy the application:
1. Download the application following the steps in the Installation section.
2. Open a prompt in the repository root folder.
3. Execute the following command:
```bash
 docker-compose up
```
3. Access the Angular web application through http://localhost:8080.

## Tests
If you want to execute the tests:
1. Download the application following the steps in the Installation section.
2. Make sure you have PHP 8 installed.
3. Open a prompt in the ```fruits-backend``` folder.
4. Execute the following command:
```bash
 php artisan test
```

## SwaggerUI
To check the API endpoints and give the API a try:
1. Deploy the application following the steps in the Deployment section.
2. Go to http://localhost:8000/api/documentation.


## Disclaimer
The icons used in this repository and in this project were made by [Eucalyp](https://www.flaticon.com/authors/eucalyp "Eucalyp") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon").