import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Fruit } from 'src/app/model/fruit.model';

@Component({
  selector: 'app-fruit-modal',
  templateUrl: './fruit-modal.component.html',
  styleUrls: ['./fruit-modal.component.css']
})
export class FruitModalComponent implements OnInit {

  @Input() title: string;
  @Input() isEdit: boolean;
  @Input() fruit: Fruit = new Fruit();

  @Output() createEvent: EventEmitter<any> = new EventEmitter();
  @Output() updateEvent: EventEmitter<any> = new EventEmitter();

  sizes = [
    {"id" : "Small", "text" : "Small"},
    {"id" : "Medium", "text" : "Medium"},
    {"id" : "Large", "text" : "Large"}
  ]

  constructor(
    public ngbActiveModal: NgbActiveModal
  ) { }

  ngOnInit(): void { }

  /**
   * Sends the fruit to be created to the event emitter
   */
  create() {
    this.createEvent.emit(this.fruit);
    this.dismiss();
  }

  /**
   * Sends the fruit to be updated to the event emitter
   */
  update() {
    this.updateEvent.emit(this.fruit);
    this.dismiss();
  }

  /**
   * Closes the modal
   */
  dismiss() {
    this.ngbActiveModal.dismiss();
  }
}
