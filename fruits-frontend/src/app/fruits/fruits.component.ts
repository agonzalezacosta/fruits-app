import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UtilsService } from '../utils/utils.service';
import { FruitModalComponent } from './fruit-modal/fruit-modal.component';
import { FruitsService } from './fruits.service';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrls: ['./fruits.component.css']
})
export class FruitsComponent implements OnInit {
  promiseList: Subscription;
  fruits: [] = [];
  page: number;
  pageSize: number;
  total: number;

  @Output() createFruitEvent = new EventEmitter<any>();
  @Output() editFruitEvent = new EventEmitter<any>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ngbModal: NgbModal,
    private fruitsService: FruitsService,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.page = params.page ? params.page : 1;

      this.find();
    });
  }

  /**
   * Retrieves a paged list of fruits
   */
  find() {
    this.promiseList = this.fruitsService.query(this.page).subscribe((data: any) => {
      this.fruits = data.data;
      this.page = data.meta.current_page;
      this.pageSize = parseInt(data.meta.per_page);
      this.total = data.meta.total;
    }, error => {
      this.utilsService.showError(error);
    });
  }

  /**
   * Page change event
   */
  pageChanged() {
    let queryParams = {};
    queryParams['page'] = this.page;
    this.router.navigate(['/fruits'], { queryParams: queryParams });
  }

  /**
   * Shows the modal for creating a new fruit and creates it
   */
  openCreateFruitModal() {
    const modalRef = this.ngbModal.open(FruitModalComponent, {
      backdrop: 'static',
      windowClass: 'modal-crud'
    });
    modalRef.componentInstance.title = "Add fruit";
    modalRef.componentInstance.isEdit = false;
    modalRef.componentInstance.createEvent.subscribe((fruit) => {
      this.promiseList = this.fruitsService.create(fruit).subscribe((data: any) => {
        this.router.navigate([`/fruits/${data.data.id}`]);
      }, error => {
        this.utilsService.showError(error);
      });
    });
  }

  /**
   * Navigates to the fruit detail page
   *
   * @param id
   */
  openFruitDetail(id) {
    this.router.navigate(['/fruits/' + id],);
  }
}
