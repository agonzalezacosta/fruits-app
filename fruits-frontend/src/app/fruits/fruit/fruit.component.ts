import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { Fruit } from 'src/app/model/fruit.model';
import { UtilsService } from 'src/app/utils/utils.service';
import { FruitModalComponent } from '../fruit-modal/fruit-modal.component';
import { FruitsService } from '../fruits.service';

@Component({
  selector: 'app-fruit',
  templateUrl: './fruit.component.html',
  styleUrls: ['./fruit.component.css']
})
export class FruitComponent implements OnInit {
  promiseList: Subscription;
  fruit: Fruit = new Fruit();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ngbModal: NgbModal,
    private fruitsService: FruitsService,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.getFruit(params['id']);
    });
  }

  /**
   * Retrieves a fruit
   */
  getFruit(id) {
    this.promiseList = this.fruitsService.get(id).subscribe((data: any) => {
      this.fruit = data.data;
    }, error => {
      this.utilsService.showError(error);
    });
  }

  /**
   * Return to the fruit list
   */
  returnToList() {
    let queryParams = {};
    queryParams['page'] = 1;
    this.router.navigate(['/fruits'], { queryParams: queryParams });
  }

  /**
   * Shows the modal for updating a new fruit and updates it
   */
  openUpdateFruitModal() {
    const modalRef = this.ngbModal.open(FruitModalComponent, {
      backdrop: 'static',
      windowClass: 'modal-crud'
    });
    modalRef.componentInstance.title = "Edit fruit";
    modalRef.componentInstance.isEdit = true;
    modalRef.componentInstance.fruit = Object.assign({}, this.fruit);
    modalRef.componentInstance.updateEvent.subscribe((fruit) => {
      this.promiseList = this.fruitsService.update(this.fruit.id, fruit).subscribe((data: any) => {
        this.getFruit(this.fruit.id);
      }, error => {
        this.utilsService.showError(error);
      });
    });
  }

  /**
   * Deletes a fruit
   */
  deleteFruit() {
    this.utilsService.showConfirm('Warning', 'Are you sure you want to delete this fruit?', 'bg-warning').result.then(() => {
      this.promiseList = this.fruitsService.delete(this.fruit.id).subscribe(() => {
        this.returnToList()
      }, error => {
        this.utilsService.showError(error);
      });
    }, () => null);
  }
}
