import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FruitsService {
  //public API = `${this.globals.api}/`;
  public API = "http://localhost:8000/api/";

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Returns a page list of fruits
   *
   * @param page  Page to return
   * @returns
   */
  query(page) {
    let params = {};
    params['page'] = page;
    return this.http.get(`${this.API}fruits`, {params:params});
  }

  /**
   * Retrieves a fruit
   *
   * @param id   Fruit id
   * @returns
   */
  get(id) {
    return this.http.get(`${this.API}fruits/${id}`);
  }

  /**
   * Creates a fruit
   *
   * @param fruit   Fruit to create
   * @returns
   */
  create(fruit) {
    return this.http.post(`${this.API}fruits`, fruit);
  }

  /**
   * Updates a fruit
   *
   * @param id      Id of the fruit to update
   * @param fruit   Data of the fruit to update
   * @returns
   */
  update(id, fruit) {
    return this.http.put(`${this.API}fruits/${id}`, fruit);
  }

  /**
   * Deletes a fruit
   *
   * @param id      Id of the fruit to delete
   * @returns
   */
  delete(id) {
    return this.http.delete(`${this.API}fruits/${id}`);
  }
}
