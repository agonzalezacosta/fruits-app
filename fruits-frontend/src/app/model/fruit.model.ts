export class Fruit {
  id: number;
  name: string;
  size: string;
  main_color: string;
}
