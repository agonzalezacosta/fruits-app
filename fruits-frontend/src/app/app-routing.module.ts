import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FruitComponent } from './fruits/fruit/fruit.component';
import { FruitsComponent } from './fruits/fruits.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'fruits' },
  { path: 'fruits', component: FruitsComponent },
  { path: 'fruits/:id', component: FruitComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
