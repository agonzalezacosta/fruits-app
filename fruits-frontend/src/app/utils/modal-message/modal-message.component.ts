import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-message',
  templateUrl: './modal-message.component.html'
})
export class ModalMessageComponent {
  @Input() title: string;
  @Input() titleClass: string;
  @Input() message: string;
  @Input() prompt: boolean;
  @Input() error: any;

  constructor(public ngbActiveModal: NgbActiveModal) { }
}
