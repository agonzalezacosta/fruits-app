import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalMessageComponent } from './modal-message/modal-message.component';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private ngbModal: NgbModal
  ) { }

  /**
   * Shows an error message modal
   *
   * @param error Error
   */
   showError(error: HttpErrorResponse): NgbModalRef {
    const modalRef = this.ngbModal.open(ModalMessageComponent, {backdrop: 'static'});
    modalRef.componentInstance.title = 'Error';
    modalRef.componentInstance.titleClass = 'text-white bg-danger';
    modalRef.componentInstance.message = error.error.errorMessage ? error.error.errorMessage : error.statusText;
    modalRef.componentInstance.error = null;
    modalRef.componentInstance.prompt = false;
    return modalRef;
  }

  /**
   * Shows a confirmation modal
   *
   * @param title       Title
   * @param message     Modal message
   * @param titleClass  Additional CSS class for the modal title
   */
  showConfirm(title: string, message: string, titleClass: string): NgbModalRef {
    const modalRef = this.ngbModal.open(ModalMessageComponent, {backdrop: 'static'});
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.titleClass = titleClass;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.error = null;
    modalRef.componentInstance.prompt = true;
    return modalRef;
  }
}
