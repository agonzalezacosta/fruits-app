<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class FruitTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test for the general fruit list.
     *
     * @return void
     */
    public function test_getFruits()
    {
        $this->getJson('api/fruits')
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test for a fruit list with two fruits.
     *
     * @return void
     */
    public function test_getFruitListOfSizeTwo()
    {
        $fruit1 = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $fruit2 = [
            "name"          => "Kiwi",
            "size"          => "Small",
            "main_color"    => "Brown"
        ];

        $this->postJson('api/fruits', $fruit1)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->postJson('api/fruits', $fruit2)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit1);
        $this->assertDatabaseHas('fruits', $fruit2);

        $this->getJson('api/fruits')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment($fruit1)
            ->assertJsonFragment($fruit2);
    }

    /**
     * Test for the valid fruit retrieval.
     *
     * @return void
     */
    public function test_getFruit()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $this->getJson('api/fruits/' . $response->getOriginalContent()->id)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );
    }

    /**
     * Test for trying to get a fruit that does not exist
     *
     * @return void
     */
    public function test_getNonExistingFruit()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $this->assertDatabaseMissing('fruits', $fruit);

        $this->getJson('api/fruits/123')
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * Test for the valid fruit creation, with all fields informed.
     *
     * @return void
     */
    public function test_createValidFruitWithAllFields()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);
    }

    /**
     * Test for the valid fruit creation, without main color.
     *
     * @return void
     */
    public function test_createValidFruitWithoutMainColor()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);
    }

    /**
     * Test for trying to create a fruit without name.
     *
     * @return void
     */
    public function test_createFruitWithoutName()
    {
        $fruit = [
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

    }

    /**
     * Test for trying to create a fruit without size.
     *
     * @return void
     */
    public function test_createFruitWithoutSize()
    {
        $fruit = [
            "name"          => "Banana",
            "main_color"    => "Yellow"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to create a fruit with an invalid name.
     *
     * @return void
     */
    public function test_createFruitWithAnInvalidName()
    {
        $fruit = [
            "name"          => 123,
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to create a fruit with an invalid string size.
     *
     * @return void
     */
    public function test_createFruitWithAnInvalidStringSize()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "ExtraLarge",
            "main_color"    => "Yellow"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to create a fruit with an invalid non-string size.
     *
     * @return void
     */
    public function test_createFruitWithAnInvalidNonStringSize()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => 123,
            "main_color"    => "Yellow"
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to create a fruit with an invalid main color.
     *
     * @return void
     */
    public function test_createFruitWithAnInvalidMainColor()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => 123
        ];

        $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for the valid fruit update
     *
     * @return void
     */
    public function test_updateValidFruitWithAllFields()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => "Banana",
            "size"          => "Large",
            "main_color"    => "Green"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $updatedFruit);
    }

    /**
     * Test for the valid fruit update, without main color.
     *
     * @return void
     */
    public function test_updateValidFruitWithoutMainColor()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => "Banana",
            "size"          => "Large"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $updatedFruit);
    }

    /**
     * Test for trying to update a fruit without name.
     *
     * @return void
     */
    public function test_updateFruitWithoutName()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "size"          => "Large",
            "main_color"    => "Green"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to update a fruit without size.
     *
     * @return void
     */
    public function test_updateFruitWithoutSize()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => "Banana",
            "main_color"    => "Green"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to update a fruit with an invalid name.
     *
     * @return void
     */
    public function test_updateFruitWithAnInvalidName()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => 123,
            "size"          => "Large",
            "main_color"    => "Green"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to update a fruit with an invalid string size.
     *
     * @return void
     */
    public function test_updateFruitWithAnInvalidStringSize()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => "Banana",
            "size"          => "ExtraLarge",
            "main_color"    => "Green"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to update a fruit with an invalid non-string size.
     *
     * @return void
     */
    public function test_updateFruitWithAnInvalidNonStringSize()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => "Banana",
            "size"          => 123,
            "main_color"    => "Green"
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to update a fruit with an invalid main color.
     *
     * @return void
     */
    public function test_updateFruitWithAnInvalidMainColor()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $updatedFruit = [
            "name"          => "Banana",
            "size"          => "Large",
            "main_color"    => 123
        ];

        $this->putJson('api/fruits/' . $response->getOriginalContent()->id, $updatedFruit)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test for trying to update a fruit that does not exist
     *
     * @return void
     */
    public function test_updateNonExistingFruit()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $this->assertDatabaseMissing('fruits', $fruit);

        $this->putJson('api/fruits/123', $fruit)
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * Test for deleting an existing fruit
     *
     * @return void
     */
    public function test_deleteExistingFruit()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $response = $this->postJson('api/fruits', $fruit)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'size',
                        'main_color',
                        'created_at',
                        'updated_at'
                    ]
                ]
            );

        $this->assertDatabaseHas('fruits', $fruit);

        $this->deleteJson('api/fruits/' . $response->getOriginalContent()->id, $fruit)
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('fruits', $fruit);
    }

    /**
     * Test for trying to delete a fruit that does not exist
     *
     * @return void
     */
    public function test_deleteNonExistingFruit()
    {
        $fruit = [
            "name"          => "Banana",
            "size"          => "Medium",
            "main_color"    => "Yellow"
        ];

        $this->assertDatabaseMissing('fruits', $fruit);

        $this->deleteJson('api/fruits/123', $fruit)
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
