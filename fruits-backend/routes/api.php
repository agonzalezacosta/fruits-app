<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FruitController;

/**
 * Fruit routes
 */
Route::get('/fruits', [FruitController::class, 'findFruits']);
Route::get('/fruits/{id}', [FruitController::class, 'getFruit']);
Route::post('/fruits', [FruitController::class, 'createFruit']);
Route::put('/fruits/{id}', [FruitController::class, 'updateFruit']);
Route::delete('/fruits/{id}', [FruitController::class, 'deleteFruit']);
