<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fruit extends Model
{
    use HasFactory;

    public const SIZES = ['Small', 'Medium', 'Large'];

    protected $fillable = [
        'name',
        'size',
        'main_color',
    ];
}
