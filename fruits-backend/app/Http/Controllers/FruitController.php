<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use App\Http\Resources\FruitCollection;
use App\Http\Resources\FruitResource;
use App\Models\Fruit;

class FruitController extends Controller
{
    /**
     *  @OA\Get(
     *      path="/api/fruits",
     *      summary="Returns a paged list of fruits",
     *      tags={"Fruits"},
     *      @OA\Parameter(
     *          description="Page number",
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Paged list of fruits."
     *      ),
     *      @OA\Response(
     *          response="500",
     *          description="Internal Server Error."
     *      )
     * )
     */
    public function findFruits()
    {
        return new FruitCollection(Fruit::paginate(env("DEFAULT_FRUIT_PAGE_SIZE", 10)));
    }

    /**
     * @OA\Get(
     *      path="/api/fruits/{id}",
     *      summary="Returns a fruit",
     *      tags={"Fruits"},
     *      @OA\Parameter(
     *          description="Fruit id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Returns a fruit."
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not Found."
     *      ),
     *      @OA\Response(
     *          response="500",
     *          description="Internal Server Error."
     *      )
     * )
     */
    public function getFruit($id)
    {
        return new FruitResource(Fruit::findOrFail($id));
    }

    /**
     * @OA\Post(
     *      path="/api/fruits",
     *      summary="Creates a fruit",
     *      tags={"Fruits"},
     *      @OA\Parameter(
     *          description="Fruit name",
     *          name="name",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Fruit size",
     *          name="size",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              enum={"Small", "Medium", "Large"}
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Fruit main color",
     *          name="main_color",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Returns the created fruit."
     *      ),
     *      @OA\Response(
     *          response="500",
     *          description="Internal Server Error."
     *      )
     * )
     */
    public function createFruit(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:200',
            'size' => ['required', 'string', Rule::in(Fruit::SIZES)],
            'main_color' => 'nullable|string|max:100'
        ]);

        $fruit = Fruit::create($request->all());

        return (new FruitResource($fruit))
                ->response()
                ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Put(
     *      path="/api/fruits/{id}",
     *      summary="Updates a fruit",
     *      tags={"Fruits"},
     *      @OA\Parameter(
     *          description="Fruit id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Fruit name",
     *          name="name",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Fruit size",
     *          name="size",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              enum={"Small", "Medium", "Large"}
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Fruit main color",
     *          name="main_color",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Returns the updated fruit."
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not Found."
     *      ),
     *      @OA\Response(
     *          response="500",
     *          description="Internal Server Error."
     *      )
     * )
     */
    public function updateFruit(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:200',
            'size' => ['required', 'string', Rule::in(Fruit::SIZES)],
            'main_color' => 'nullable|string|max:100'
        ]);

        $fruit = Fruit::findOrFail($id);

        $fruit->name = $request->input('name');
        $fruit->size = $request->input('size');
        $fruit->main_color = $request->input('main_color');
        $fruit->save();

        return (new FruitResource($fruit))
                ->response()
                ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Delete(
     *      path="/api/fruits/{id}",
     *      summary="Deletes a fruit",
     *      tags={"Fruits"},
     *      @OA\Parameter(
     *          description="Fruit id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Returns nothing."
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not Found."
     *      ),
     *      @OA\Response(
     *          response="500",
     *          description="Internal Server Error."
     *      )
     * )
     */
    public function deleteFruit($id)
    {
        $fruit = Fruit::findOrFail($id);
        $fruit->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
