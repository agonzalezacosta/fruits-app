<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
*   @OA\Info(
*       title="FruitsAPP API",
*       description="FruitsAPP Swagger Documentation",
*       version="1.0.0"
*   )
*
*   @OA\Server(
*       url=L5_SWAGGER_APP_SERVER,
*       description="API Server"
*   )
*
*   @OA\Tag(
*       name="Fruits",
*       description="Fruits API Endpoints"
*   )
*/
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
